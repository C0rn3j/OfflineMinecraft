This a walk-through to set-up a safe, completely offline Minecraft client and server instance without loss of functionality like skins.

It is required to have access to a Mojang account that owns Minecraft so you can download the initial game files.

Steps are for Linux, although you can easily adapt paths and other things for Windows/macOS.

## Client
  * Get Prism Launcher (MultiMC successor) https://prismlauncher.org/download
  * Add instance of Minecraft in Prism with the version you want to use, selecting Forge as the modloader
  * Add account info to Prism and launch the instance to download the requires files
  * If you're like me, you have mods in a separate folder, so symlink the mods folder of the instance to your folder. `rmdir ~/.local/share/multimc/instances/1.19.2/.minecraft/mods; ln -s ~/Nextcloud/HugeFiles/Minecraft/Minecraft\ 1.19.2/mods ~/.local/share/PrismLauncher/instances/1.19.2/.minecraft/mods`
  * Add [OfflineSkins](https://github.com/zlainsama/OfflineSkins) to the mod folder, this makes skins and other skin related features work in offline mode.
  * Host a webserver that returns files upon requesting uuid/nickname. For example if you requested server.com/skins/C0rn3j it should return the skin and if you requested server.com/capes/C0rn3j it should return the cape.
  * Add offlineskins.cfg to config, use the file in this repo as an example.
  * Replace accounts.json in ~/.local/share/multimc with the one in this repository. This will ensure it works in offline mode without a working Mojang account.
  * Set a non-existing proxy inside MultiMC to make sure all checks fail or block authserver.mojang.com in the hosts file. Otherwise MultiMC will remove the broken accesstoken
  * Copy the whole ~/.local/share/multimc folder to whatever machines you want, it should be portable. You're done with the client side!
  * When in game press O and disable autojump, step assist from Everlasting Abilities is better.
  * You can hold ~ to use veinminer. English layout.

## Server
  * Run [Forge Installer](https://files.minecraftforge.net/) and install the server somewhere (`~/minecraftServer` in the example here): `java -jar forge-*-installer.jar --installServer`
  * Add startServer.sh to the server directory, modify the forge filename if needed
  * As per client, symlink mods. `rmdir ~/minecraftServer/mods; ln -s ~/Nextcloud/HugeFiles/Minecraft/Minecraft\ 1.19.2/mods ~/minecraftServer/mods`
  * Launch the server via ./startServer.sh
  * Execute `gamerule mobGriefing false` in the server console
  * Execute `gamerule playersSleepingPercentage 0` too to allow a single player to sleep through the night
  * Stop the server and restart the script to apply config changes
