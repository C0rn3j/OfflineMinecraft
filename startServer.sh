#!/bin/bash
set -euo pipefail

#RAMAmount="8G"

# Autoaccept EULA
if [[ $(cat eula.txt 2>/dev/null) != "eula=true" ]]; then
	echo "eula.txt not found or not in correct format, (re)creating it..."
	echo "eula=true" > eula.txt
fi
## Veinminer automine everything with any item - replaced by OreExcavation
#if [[ -e ./config/veinminer/general.cfg ]]; then
#	if grep "B:override.allBlocks=false" config/veinminer/general.cfg >/dev/null; then
#		echo "Setting veinminer to automine everything with any item..."
#		sed -i s/B:override.allBlocks=false/B:override.allBlocks=true/g ./config/veinminer/general.cfg
#		sed -i s/B:override.allTools=false/B:override.allTools=true/g ./config/veinminer/general.cfg
#	fi
#fi

# TODO globalwaystonesetuprequirescreativemode=false
if [[ -e ./server.properties ]]; then
	if grep "allow-flight=false" server.properties >/dev/null; then
		echo "Allowing flight since some mods trigger the anticheat check..."
		sed -i s/"allow-flight=false"/"allow-flight=true"/g server.properties
	fi
	if grep "online-mode=true" server.properties >/dev/null; then
		echo "Disable online mode so the clients don't hang because of token refreshes and whatnot..."
		sed -i s/"online-mode=true"/"online-mode=false"/g server.properties
	fi
	if grep "spawn-protection=16" server.properties >/dev/null; then
		echo "Remove spawn protection to prevent weird issues..."
		sed -i s/"spawn-protection=16"/"spawn-protection=0"/g server.properties
	fi
fi
## Nerf everlasting abilities to only have step assist. Everything else is OP.
#if [[ -e ./config/everlastingabilities-common.toml ]]; then
#	if grep "TODO" ./config/everlastingabilities-common.toml >/dev/null; then
#		echo TODO
#	fi
#fi
if [[ -e config/theoneprobe-common.toml ]]; then
	if grep "needsProbe = 3" ./config/theoneprobe-common.toml >/dev/null; then
		echo "Removing the need for a probe to see extended info..."
		sed -i 's/needsProbe = 3/needsProbe = 0/g' config/theoneprobe-common.toml
	fi
fi

# Main cycle
while :; do
	echo "Starting Minecraft server..."
#	java -jar -Xms${RAMAmount} -Xmx${RAMAmount} forge-*[0-9].jar nogui
	java @user_jvm_args.txt @libraries/net/minecraftforge/forge/1.19.2-43.2.9/unix_args.txt "$@"
	echo "Minecraft server exited, waiting 10 seconds before re-launching it..."
	sleep 10
done
